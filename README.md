# Source files for platfrastructure.life
These are the source files for the website [platfrastructure.life](https://platfrastructure.life)

Site is built by Gitlab CI and deployed to Gitlab pages. See the [CI configuration](https://gitlab.com/tedwardd/platfralife/blob/master/.gitlab-ci.yml]) for specific implementation details
