---
title: "Sports in my Life"
date: 2019-03-20T17:21:45-04:00
categories: ["Blog"]
description: ""
draft: false
tags: ["sports", "triathlon", "bike", "swim", "run"]
images: []
toc: false
---

Little known fact about me, I like sports. It feels kind of risqué admitting it
to an audience no doubt made up of mostly technologists, geeks and nerds; the
very people with whom I regularly make "sports ball" jokes with, but it's true.
But I'm not your typical sports fan. Sure, I watch the Superbowl. Yes, I enjoy
going to see a baseball game with friends once in a while (maybe < once a year
in reality) but those aren't my passion. I like individual sports. My personal
favorites to watch are speed skating and cycling. Coincidentally, those also
happen to be two that I compete in. SURPRISE, I don't sit in front of my
computer all day. In fact, of all the things I do in life, that's probably my
least favorite.

One of the earliest memories I have as a child is my parents teaching me to ride
a bike. Some years later, maybe when I was somewhere between 8-11 years old, I
discovered the world of inline skating. I've felt at home both on two and four
wheels ever since. It wasn't until I was about 17 years old, however, that I
really found an interest in riding bikes (more than just as a way to get to a
friend's house at least). I spent the junior and senior years of high school
doing basically two things outside of school: biking and discovering the
Internet. I would come home from school, immediately get on my bicycle and ride
for hours. I'd come home in time for family dinner, do my homework (maybe) and
then I was upstairs in my "cave", online on IRC, message boards, you name it. I
would typically be up until 2, 3AM, sometimes later. Up for school the next day
at 6AM, repeat.

It wasn't until last year, however, that I thought that maybe I should try this
racing thing. I'd always just enjoyed skating or biking for their own right. I
never felt compelled to prove I was better than another person. Something in me
has changed in the last year. While I still don't feel that competitive drive to
beat another person, I want to beat myself; to prove that I can consistently
improve on my own times and my own achievements. This has driven me to make
triathlon my sport of choice this year.

I view triathlon as a sport in which you prove your capabilities only to
yourself. Sure you can be faster than another person, you can win the race. But
so many people compete not against others but against the clock , against
themselves in this sport. It feels like a natural fit. This year, I have no
real goals other than to be consistent. I'm on a structured training plan which
has already started to really help me improve. I also have a couple of races
lined up for the year, just something to motivate me to stay on target with my
training.

So, I'm going to try to keep the tags straight on the posts but you might start
seeing some non-tech posts popping up every now and again. Hopefully you'll
stick around for the journey.
