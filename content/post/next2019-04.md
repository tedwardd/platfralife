---
title: "Google Next 2019: Day 4"
date: 2019-04-12T01:22:54-04:00
categories: []
description: ""
draft: false
tags: []
images: []
toc: false
---

Day four, the final day. Well, the final day of the actual conference, anyway.
The day started like the others. Wake up, go for a run, grab some coffee on the
walk to the conference. Probably the best part of the day, to be honest. Not
that today was particularly bad or anything but it's hard to beat coffee and
stroll through downtown SF.

The first talk on the schedule today was a security track about identity
management in GCP. Unfortunately, due to timezone struggles, I slept in too late
and didn't make it. I'm sure the talk was great but I have no ability to report
anything about it.

I had two other talks scheduled for the day. One of them was at 11:40PM, _GCP:
Move Fast and Don't Break Things_. The second was 1:15PM titled _Repeatable GCP
Environments at Scale With Cloud Build Infra-As-Code Pipelines_. The astute
reader at this point is probably wondering when I got lunch in between this...
well, I had to sacrifice one of them. I went to the talk on repeatable builds.

The repeatable builds talk was actually really good. They talked about using
Terraform, as well as Deployment Manager, to define your infrastructure in code
and use cloud builds with forseti policy enforcement to do some sanity checking
and build tests before deploying to production. Overall, I came away with a few
nice tidbits of information that will hopefully help improve our overall GCP
experience.

After the talks were over the team went out for drinks with some Googlers. Then
a few of us went over to Berkeley, CA for dinner.

I have one full day left in SF. I have to work from the office for the day but I
hope to have enough time to go see a few things before I leave.
