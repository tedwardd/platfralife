---
title: "Final Day in SF: Post Next 2019"
date: 2019-04-13T17:57:17-04:00
categories: []
description: ""
draft: false
tags: []
images: []
toc: false
---

This post has nothing to do with the conference or technology. It will also be
brief. I wanted to give a huge shout out to the San Francisco skating community.
I was a stranger in their community and they welcomed me with open arms. Having
spent a week in San Francisco I came to feel like most of the people there were
cold. Rarely did I experience a warm smile on the street, a friendly face. It
was very refreshing to find that not everyone in the city is like this. Maybe
it's something about people drawn to inline skating, I'm not sure, but I will be
forever thankful of the San Francisco skate community for the warm smiles, the
hugs and the laughter we shared on my final day in the city.

Friday was a work day for me. Nothing interesting to report there. The fun
started around 6PM local time. Strapped on my skates and headed out to explore.
I spent about an hour skating around, finding alleyways and hidden parts of the
city not normally found unless on foot. The city has a lot of interesting spots
to explore on skates. I highly encourage anyone visiting to try it sometime.
Just put your skates on and let the whims take you where they will. I'm sure you
won't be disappointed.

After about an hour of exploration I headed over to the Ferry Building; the
meeting spot for the Friday night skate. The skate wasn't expected to start for
a while so there was only one other skater there. We chatted and instantly
became friends. He showed me a great food spot not far from the meetup spot
where I was able to grab a quick bite before the skate started.

People started to filter not long after we got back from the food run. We danced
in the square for an hour waiting for the rest of the gang to show up. Once the
time arrived we set out for what was sure to be a fun journey across the city.
Taking about three hours, we covered 12 miles of San Francisco. We hit several
scenic sites, some great photo spots. Got to bomb a few hills and storm a few
tunnels.

After the main event concluded, about half of the group stuck around for a game
of roller hockey in the square. This was some of the most fun I've had in a long
time. We played for about an hour, concluding around 1AM. After the conclusion
of the hockey game, I spent another hour exploring the city some more. Let me
tell you, at 1AM, San Francisco, specifically the financial district, is a
skaters paradise. Some really great spots to explore and almost no cars or
pedestrians to worry about.

If you are ever in San Francisco on a Friday night, I highly encourage you to
check out the night skate. The group is very welcoming and friendly. The route
is about as easy as any route in a city of hills can be and the after skate
activities are second to none.
