+++
title = "Home"
+++

My name is Ted. The Platfrastructure Life is my personal blog and website. The
content on this site, except where noted otherwise, is original content. The
views and opinions expressed on this site are my own and in no way reflect those
of my employer.

To learn more about me, please check out my {{< menu_url >}}about{{< /menu_url >}} page
