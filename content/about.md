+++
title = "About"
date = "2024-06-21"
sidemenu = "true"
description = "Ted Wood"
+++

I am a Senior Software Engineer based in Atlanta, GA, currently in the employment of
[Intuit Mailchimp](https://mailchimp.com). Views expressed here are my own and
do not reflect those of my employer or anyone else

## Personal Passions

My hobbies and passions change with the wind. I've tried to list some of the
longer surviving ones here.

* Cycling
* Bicycle Fabrication and Restoration
* Vintage computer hardware
* Video games
* Model building
* Working on cars

## Professional Interests

Like my personal passions, the professional interests are subject to change with
the ever changing needs of business. Below I've attempted to compile a list of
some of the technologies that have stuck around in my professional life for more
than a fleeting moment.

* Linux
* Kubernetes
* Go
* Terraform
* Hugo (bringing you this website)

## Club/Group Memberships

I'm currently a regular member of a few local and international interest
groups. I've listed them below with links to where you can get more information
about these groups:

* [Atlanta Linux Enthusiasts](https://ale.org/)
* [Atlanta Peachtree Road Rollers](https://www.aprr.org/)
* [DevOpsATL](https://www.meetup.com/DevOpsATL/)
* [Kubernetes Atlanta](https://www.meetup.com/Kubernetes-Atlanta-Meetup/)
* [SDF](https://sdf.org)
