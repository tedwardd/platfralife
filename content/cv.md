+++
title = "Ted Wood"
date = "2022-11-11"
sidemenu = "true"
description = "Ted.L.Wood@gmail.com"
+++

Systems Engineer
-------------------

Self taught and motivated professional consistently recognized for providing
high level technical expertise and creative solutions to hard, large scale
problems. Proficiency with Kubernetes, Unix operating systems and Puppet as
well as Jenkins, Terraform, and Python for automating cloud infrastructure.

Business Skills
---------------------------------------

#### Software Development

**Primary language skills**: Go, Python, Bash

Good working knowledge of Go. I've written internal tooling, Slack/Discord
bots, CI tools and other integrations. I've recently begun writing more APIs in
Go using twirp, grpc and protobufs. While this area is still new to me, it's
definitely an area I find interesting and would love to continue to explore.

Several years of working in Python for day-to-day projects and application
development. Professionally, I've used Python to develop everything from web
applications using Django to gluing disperate backend APIs and cloud services
together. Personally, I've used Python to create home automation services,
automate the scraping and wrangling of large data sets and data streams for
crypto exchanges and other third party financial platforms for high frequency
trading activity, as well as created bots for platforms such as Discord and
Slack.


#### Terraform and Google Cloud

I was the Lead Engineer in the development of a maintainable and scalable
solution for cloud resources management within GCP. Extensive experience
working with Terraform in conjuction with Atlantis, Terraform Enterprise,
Python, and Jenkins to automate the testing and deployment of infrastructure
code maintainined in a large shared git repository.

#### Kubernetes

Member of a small team responsible for developing and driving the adoption of
Kuberentes and service based architecture as a replacement for an aging
monolithic application and system stack.

#### Puppet

Used puppet to manage a fleet of over 2000 physical servers across multiple
datacenters. Comfortable with all aspects of puppet management from module developement
to the creation and maintenance of roles and profiles for a variety of system
classifications.

#### Unix Operating Systems

20 years of home Linux use and 12 years of professional use. Primary familiarity
with RedHat Enterprise Linux (CentOS) and secondary with Ubuntu Linux. Intermediate
experience with FreeBSD for use of ZFS based network file storage.

Professional Experience
-----------------------

#### Intuit Mailchimp
*Senior Software Engineer* -- 2016 - present

 * Develop tools and APIs to assist engineers in building and maintaining cloud and on-prem services
 * Lead engineer on cloud infrastructure greenfield project
 * Key member of the team leading Kubernetes on-prem adoption
 * Provision, maintain and automate large scale infrastructure powering mailchimp.com
 * Mentor junior engineers and interns
 * Recognizing and breaking down large scale organization-level problems

#### Georgia Tech Research Institute
*Senior IT Support Professional* -- 2011 - 2016

 * Maintain the Linux servers and desktops used by a team of over 100 software engineers
 * Manage interns and coop student assistants
 * Develop automation workflows for securing and validating DoD secure systems

#### CareStream Dental Systems
*Helpdesk Support* -- 2009 - 2011

 * Field technical support calls for Carestream Dental Systems Customers
 * Maintain "unofficial" internal XMPP chat server

#### iLearn Inc.
*QA and Technical Support* -- 2008 - 2009

 * Develop QA and Technical Support procedures for a small online learning software company

Personal Interests
------------------

* Endurance sports (cycling/triathlon)
* Making (metal fabrication and 3D printing)
* Speed skating (wheels, not ice)
* Science Fiction Literature
* Computer History

External Links
--------------

* [My Github](https://github.com/tedwardd)
* [Gitlab](https://gitlab.com/tedwardd)
* [Personal Blog](http://platfrastructure.life)
